<?php
/**
 * Desktop Nova Internet - mu-plugin
 *
 * PHP version 7
 *
 * @category  Wordpress_Mu-plugin
 * @package   Desktop
 * @author    WeDo Digital <contato@wedodigital.com.br>
 * @copyright 2020 WeDo Digital
 * @license   Proprietary https://wedodigital.com.br
 * @link      https://wedodigital.com.br
 *
 * @wordpress-plugin
 * Plugin Name: Desktop Nova Internet - mu-plugin
 * Plugin URI:  https://wedodigital.com.br
 * Description: Customizations for wedodigital.com.br site
 * Version:     1.0.0
 * Author:      WeDo Digital
 * Author URI:  https://wedodigital.com.br/
 * Text Domain: desktopnovainternet
 * License:     Proprietary
 * License URI: https://wedodigital.com.br
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

/**
 * Load Translation
 *
 * @return void
 */
add_action(
    'plugins_loaded',
    function () {
        load_muplugin_textdomain('desktopnovainternetcombr', basename(dirname(__FILE__)).'/languages');
    }
);

/**
 * Hide editor from all pages
 */
add_action(
    'admin_init',
    function () {
        remove_post_type_support('page', 'editor');
    }
);

/***********************************************************************************
 * Callback Functions
 **********************************************************************************/

 /**
  * Metabox for Page Slug
  *
  * @param bool  $display  Display
  * @param array $meta_box Metabox 
  *
  * @return bool $display
  *
  * @author Tom Morton
  * @link   https://github.com/CMB2/CMB2/wiki/Adding-your-own-show_on-filters
  */
function Cmb2_Metabox_Show_On_slug($display, $meta_box)
{
    if (!isset($meta_box['show_on']['key'], $meta_box['show_on']['value'])) {
        return $display;
    }

    if ('slug' !== $meta_box['show_on']['key']) {
        return $display;
    }

    $post_id = 0;

    // If we're showing it based on ID, get the current ID
    if (isset($_GET['post'])) {
        $post_id = $_GET['post'];
    } elseif (isset($_POST['post_ID'])) {
        $post_id = $_POST['post_ID'];
    }

    if (!$post_id) {
        return $display;
    }

    $slug = get_post($post_id)->post_name;

    // See if there's a match
    return in_array($slug, (array) $meta_box['show_on']['value']);
}
add_filter('cmb2_show_on', 'Cmb2_Metabox_Show_On_slug', 10, 2);

/**
 * Gets a number of terms and displays them as options
 *
 * @param CMB2_Field $field CMB2 Field
 *
 * @return array An array of options that matches the CMB2 options array
 */
function Cmb2_getTermOptions($field)
{
    $args = $field->args('get_terms_args');
    $args = is_array($args) ? $args : array();

    $args = wp_parse_args($args, array('taxonomy' => 'category'));

    $taxonomy = $args['taxonomy'];

    $terms = (array) cmb2_utils()->wp_at_least('4.5.0')
        ? get_terms($args)
        : get_terms($taxonomy, $args);

    // Initate an empty array
    $term_options = array();
    if (!empty($terms)) {
        foreach ($terms as $term) {
            $term_options[ $term->term_id ] = $term->name;
        }
    }
    return $term_options;
}

/***********************************************************************************
 * Register post types
 * ********************************************************************************/

/**
 * Combos
 */
//if (!post_type_exists('desktop_combos')) {
    function cpt_desktopnovainternetcombr_combos() 
    {
        $labels = array(
                'name'                  => __('Combos', 'desktopnovainternetcombr'),
                'singular_name'         => __('Combo', 'desktopnovainternetcombr'),
                'menu_name'             => __('Combos', 'desktopnovainternetcombr'),
                'name_admin_bar'        => __('Combo', 'desktopnovainternetcombr'),
                'archives'              => __('Combo Archives', 'desktopnovainternetcombr'),
                'attributes'            => __('Combo Attributes', 'desktopnovainternetcombr'),
                'parent_item_colon'     => __('Parent Combo:', 'desktopnovainternetcombr'),
                'all_items'             => __('All Combos', 'desktopnovainternetcombr'),
                'add_new_item'          => __('Add New Combo', 'desktopnovainternetcombr'),
                'add_new'               => __('Add new _combo', 'desktopnovainternetcombr'),
                'new_item'              => __('New Combo', 'desktopnovainternetcombr'),
                'edit_item'             => __('Edit Combo', 'desktopnovainternetcombr'),
                'update_item'           => __('Update Combo', 'desktopnovainternetcombr'),
                'view_item'             => __('View Combo', 'desktopnovainternetcombr'),
                'view_items'            => __('View Combos', 'desktopnovainternetcombr'),
                'search_items'          => __('Search Combo', 'desktopnovainternetcombr'),
                'not_found'             => __('Not found', 'desktopnovainternetcombr'),
                'not_found_in_trash'    => __('Not found in Trash', 'desktopnovainternetcombr'),
                'featured_image'        => __('Featured Image', 'desktopnovainternetcombr'),
                'set_featured_image'    => __('Set featured image', 'desktopnovainternetcombr'),
                'remove_featured_image' => __('Remove featured image', 'desktopnovainternetcombr'),
                'use_featured_image'    => __('Use as featured image', 'desktopnovainternetcombr'),
                'insert_into_item'      => __('Insert into combo', 'desktopnovainternetcombr'),
                'uploaded_to_this_item' => __('Uploaded to this combo', 'desktopnovainternetcombr'),
                'items_list'            => __('Combos list', 'desktopnovainternetcombr'),
                'items_list_navigation' => __('Combos list navigation', 'desktopnovainternetcombr'),
                'filter_items_list'     => __('Filter Combos list', 'desktopnovainternetcombr'),
        );
        $rewrite = array(
                'slug'                  => __('combos', 'desktopnovainternetcombr'),
                'with_front'            => true,
                'pages'                 => true,
                'feeds'                 => true,
        );
        $args = array(
                'label'                 => __('Combos', 'desktopnovainternetcombr'),
                'description'           => __('Desktop Combos', 'desktopnovainternetcombr'),
                'labels'                => $labels,
                'supports'              => array('title', 'revisions', 'page-attributes'),
                'taxonomies'            => array(''),
                'hierarchical'          => false,
                'public'                => true,
                'show_ui'               => true,
                'show_in_menu'          => true,
                'menu_position'         => 5,
                'menu_icon'             => 'data:image/svg+xml;base64,'. base64_encode('<svg viewBox="0 0 32 26" fill="none" xmlns="http://www.w3.org/2000/svg">                <path d="M29.6 0H2.4C1.075 0 0 1.075 0 2.4V18.4C0 19.725 1.075 20.8 2.4 20.8H14.655V22.4H6.655C5.77 22.4 5.055 23.115 5.055 24C5.055 24.885 5.77 25.6 6.655 25.6H25.855C26.74 25.6 27.455 24.885 27.455 24C27.455 23.115 26.74 22.4 25.855 22.4H17.855V20.8H29.6C30.925 20.8 32 19.725 32 18.4V2.4C32 1.075 30.925 0 29.6 0ZM28.8 17.6H3.2V3.2H28.8V17.6Z" fill="black"/></svg>'),
                'show_in_admin_bar'     => false,
                'show_in_nav_menus'     => true,
                'can_export'            => true,
                'has_archive'           => false,
                'exclude_from_search'   => false,
                'publicly_queryable'    => true,
                'rewrite'               => $rewrite,
                'capability_type'       => 'page',
        );
        register_post_type('desktop_combos', $args); 
    } 
    add_action('init', 'cpt_desktopnovainternetcombr_combos', 0);
//}

/**
 * Internet Plans
 */
//if (!post_type_exists('desktop_internet')) {
    function cpt_desktopnovainternetcombr_internet() 
    {
        $labels = array(
            'name'                  => __('Internet Plans', 'desktopnovainternetcombr'),
            'singular_name'         => __('Internet Plan', 'desktopnovainternetcombr'),
            'menu_name'             => __('Internet Plans', 'desktopnovainternetcombr'),
            'name_admin_bar'        => __('Internet Plan', 'desktopnovainternetcombr'),
            'archives'              => __('Internet Plan Archives', 'desktopnovainternetcombr'),
            'attributes'            => __('Internet Plan Attributes', 'desktopnovainternetcombr'),
            'parent_item_colon'     => __('Parent Internet Plan:', 'desktopnovainternetcombr'),
            'all_items'             => __('All Internet Plans', 'desktopnovainternetcombr'),
            'add_new_item'          => __('Add New Internet Plan', 'desktopnovainternetcombr'),
            'add_new'               => __('Add new _internet plan', 'desktopnovainternetcombr'),
            'new_item'              => __('New Internet Plan', 'desktopnovainternetcombr'),
            'edit_item'             => __('Edit Internet Plan', 'desktopnovainternetcombr'),
            'update_item'           => __('Update Internet Plan', 'desktopnovainternetcombr'),
            'view_item'             => __('View Internet Plan', 'desktopnovainternetcombr'),
            'view_items'            => __('View Internet Plans', 'desktopnovainternetcombr'),
            'search_items'          => __('Search Internet Plan', 'desktopnovainternetcombr'),
            'not_found'             => __('Not found', 'desktopnovainternetcombr'),
            'not_found_in_trash'    => __('Not found in Trash', 'desktopnovainternetcombr'),
            'featured_image'        => __('Featured Image', 'desktopnovainternetcombr'),
            'set_featured_image'    => __('Set featured image', 'desktopnovainternetcombr'),
            'remove_featured_image' => __('Remove featured image', 'desktopnovainternetcombr'),
            'use_featured_image'    => __('Use as featured image', 'desktopnovainternetcombr'),
            'insert_into_item'      => __('Insert into internet plan', 'desktopnovainternetcombr'),
            'uploaded_to_this_item' => __('Uploaded to this internet plan', 'desktopnovainternetcombr'),
            'items_list'            => __('Internet Plans list', 'desktopnovainternetcombr'),
            'items_list_navigation' => __('Internet Plans list navigation', 'desktopnovainternetcombr'),
            'filter_items_list'     => __('Filter Internet Plans list', 'desktopnovainternetcombr'),
        );
        $rewrite = array(
                'slug'                  => __('internet', 'desktopnovainternetcombr'),
                'with_front'            => true,
                'pages'                 => true,
                'feeds'                 => true,
        );
        $args = array(
                'label'                 => __('Internet Plans', 'desktopnovainternetcombr'),
                'description'           => __('Desktop Internet Plans', 'desktopnovainternetcombr'),
                'labels'                => $labels,
                'supports'              => array('title', 'revisions', 'page-attributes'),
                'taxonomies'            => array(''),
                'hierarchical'          => false,
                'public'                => true,
                'show_ui'               => true,
                'show_in_menu'          => true,
                'menu_position'         => 5,
                'menu_icon'             => 'data:image/svg+xml;base64,'. base64_encode('<svg viewBox="0 0 32 23" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M19.2255 19.3536C19.2255 21.135 17.7814 22.5791 16 22.5791C14.2186 22.5791 12.7744 21.135 12.7744 19.3536C12.7744 17.5722 14.2186 16.128 16 16.128C17.7814 16.128 19.2255 17.5722 19.2255 19.3536ZM26.113 13.0818C26.4388 12.7559 26.4277 12.2217 26.0846 11.9142C20.3495 6.77422 11.6439 6.78006 5.91537 11.9142C5.5722 12.2217 5.56111 12.7559 5.88694 13.0818L7.60082 14.7956C7.89953 15.0944 8.38019 15.1142 8.69795 14.8358C12.875 11.1763 19.1343 11.1845 23.302 14.8358C23.6198 15.1142 24.1004 15.0944 24.3991 14.7956L26.113 13.0818ZM31.7632 7.35048C32.085 7.02868 32.0784 6.50297 31.7456 6.19266C22.8871 -2.06573 9.10966 -2.06271 0.254392 6.19266C-0.078444 6.50297 -0.0850462 7.02873 0.236752 7.35048L1.94861 9.06234C2.25579 9.36952 2.7495 9.37637 3.06868 9.08164C10.3646 2.34489 21.6335 2.34318 28.9313 9.08164C29.2504 9.37637 29.7442 9.36952 30.0513 9.06234L31.7632 7.35048Z" fill="#212121"/></svg>'),
                'show_in_admin_bar'     => false,
                'show_in_nav_menus'     => true,
                'can_export'            => true,
                'has_archive'           => false,
                'exclude_from_search'   => false,
                'publicly_queryable'    => true,
                'rewrite'               => $rewrite,
                'capability_type'       => 'page',
    );
        register_post_type('desktop_internet', $args); 
    } 
    add_action('init', 'cpt_desktopnovainternetcombr_internet', 0);
//}

/***********************************************************************************
 * Pages Custom Fields
 * ********************************************************************************/

/**
 * Front-page
 */
add_action(
    'cmb2_admin_init',
    function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_desktopnovainternetcombr_frontpage_';

        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => 'desktopnovainternetcombr_frontpage_hero_id',
                'title'         => __('Hero', 'desktopnovainternetcombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
                
        //Hero Group
        $hero_id = $cmb_hero->add_field(
            array(
                'id'          => $prefix . 'hero_slides',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   =>__('Slide {#}', 'desktopnovainternetcombr'),
                    'add_button'   =>__('Add Another Slide', 'desktopnovainternetcombr'),
                    'remove_button' =>__('Remove Slide', 'desktopnovainternetcombr'),
                    'sortable'      => true, // beta
                ),
            )
        );

        //Background Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Background Color', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#fbfbfb', '#ae0f0a', '#fbb900'),
                        ),
                    ),
                ),
            )
        );

        //Hero Background Image (Desktop)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Desktop)', 'desktopnovainternetcombr'),
                'description' => '',
                'id'          => 'bkg_img_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'desktopnovainternetcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero Background Image (Mobile)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Mobile)', 'desktopnovainternetcombr'),
                'description' => '',
                'id'          => 'bkg_img_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'desktopnovainternetcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero CSS Gradient
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('CSS Gradient', 'desktopnovainternetcombr'),
                'desc'       => 'linear-gradient(270deg, #ac2407 0%, rgba(179, 85, 65, 0) 10.5%, rgba(179, 85, 65, 0) 89.5%, #ac2407 100%);',
                'id'         => 'css_gradient',
                'type'       => 'textarea_code',
            )
        );

        //Hero Subtitle
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Subtitle', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'subtitle',
                'type'       => 'text_medium',
            )
        );

        //Hero Subtitle Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Subtitle Color', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'subtitle_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#212121', '#ae0f0a', '#fbb900'),
                        ),
                    ),
                ),
            )
        );

        //Hero Title
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Title', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'title',
                'type'       => 'textarea_code',
            )
        );

        //Hero Subtitle Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Title Color', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'title_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#212121', '#ae0f0a', '#fbb900'),
                        ),
                    ),
                ),
            )
        );

        //Hero Image Inner (Desktop)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Image Inner (Desktop)', 'desktopnovainternetcombr'),
                'description' => __('PNG with transparent background', 'desktopnovainternetcombr'),
                'id'          => 'img_inner_desktop',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'desktopnovainternetcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );
        
        //Hero Image Inner (Mobile)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Image Inner (Mobile)', 'desktopnovainternetcombr'),
                'description' => __('PNG with transparent background', 'desktopnovainternetcombr'),
                'id'          => 'img_inner_mobile',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'desktopnovainternetcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );
        //Hero Button Text
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Button Text', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'btn_text',
                'type'       => 'text_medium',
            )
        );

        //Hero Button URL
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Button URL', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'btn_url',
                'type'       => 'text',
            )
        );
        
        //Hero Button CSS classes
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Button CSS Classes', 'desktopnovainternetcombr'),
                'desc'       => __('Colors: wedo-c-button--primary, wedo-c-button--secondary, wedo-c-button--white / Types: wedo-c-button--solid / wedo-c-button--big', 'desktopnovainternetcombr'),
                'id'         => 'btn_css',
                'default'    => 'wedo-c-button--secondary wedo-c-button--solid',
                'type'       => 'text',
            )
        );

        /******
         * Combos
         ******/
        $cmb_combos = new_cmb2_box(
            array(
                'id'            => 'desktopnovainternetcombr_frontpage_combos_id',
                'title'         => __('Combos', 'desktopnovainternetcombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Combo Title
        $cmb_combos->add_field(
            array(
                'name'       => __('Title', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'combos_title',
                'type'       => 'text',
            )
        );

        //Combo Subtitle
        $cmb_combos->add_field(
            array(
                'name'       => __('Subtitle', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'combos_subtitle',
                'type'       => 'textarea_code',
            )
        );

        /******
         * Internet
         ******/
        $cmb_internet = new_cmb2_box(
            array(
                'id'            => 'desktopnovainternetcombr_frontpage_internet_id',
                'title'         => __('Internet', 'desktopnovainternetcombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Internet Title
        $cmb_internet->add_field(
            array(
                'name'       => __('Title', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'internet_title',
                'type'       => 'text',
            )
        );

        //Internet Subtitle
        $cmb_internet->add_field(
            array(
                'name'       => __('Subtitle', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'internet_subtitle',
                'type'       => 'textarea_code',
            )
        );

        /******
         * Footer
         ******/
        $cmb_footer = new_cmb2_box(
            array(
                'id'            => 'desktopnovainternetcombr_frontpage_footer_id',
                'title'         => __('Footer', 'desktopnovainternetcombr'),
                'object_types'  => array('page'), // post type
                'show_on'       => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Footer Text
        $cmb_footer->add_field(
            array(
                'name'       => __('Text', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'footer_text',
                'type'       => 'textarea_code',
            )
        );

        //Telesales Text
        $cmb_footer->add_field(
            array(
                'name'       => __('Telesales Text', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'footer_telesales_text',
                'type'       => 'textarea_code',
            )
        );
    }
);

/**
 * Combos Page
 */
add_action(
    'cmb2_admin_init',
    function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_desktopnovainternetcombr_combos_';

        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => 'desktopnovainternetcombr_combos_hero_id',
                'title'         => __('Hero', 'desktopnovainternetcombr'),
                'object_types'  => array('page'), // post type
                'show_on'       => array('key' => 'slug', 'value' => 'combos'),
                //'show_on'       => array( 'key' => 'page-template', 'value' => 'page-combos.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
                
        //Hero Group
        $hero_id = $cmb_hero->add_field(
            array(
                'id'          => $prefix . 'hero_slides',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   =>__('Slide {#}', 'desktopnovainternetcombr'),
                    'add_button'   =>__('Add Another Slide', 'desktopnovainternetcombr'),
                    'remove_button' =>__('Remove Slide', 'desktopnovainternetcombr'),
                    'sortable'      => true, // beta
                ),
            )
        );

        //Background Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Background Color', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#fbfbfb', '#ae0f0a', '#fbb900'),
                        ),
                    ),
                ),
            )
        );

        //Hero Background Image (Desktop)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Desktop)', 'desktopnovainternetcombr'),
                'description' => '',
                'id'          => 'bkg_img_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'desktopnovainternetcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero Background Image (Mobile)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Mobile)', 'desktopnovainternetcombr'),
                'description' => '',
                'id'          => 'bkg_img_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'desktopnovainternetcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero CSS Gradient
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('CSS Gradient', 'desktopnovainternetcombr'),
                'desc'       => 'linear-gradient(270deg, #ac2407 0%, rgba(179, 85, 65, 0) 10.5%, rgba(179, 85, 65, 0) 89.5%, #ac2407 100%);',
                'id'         => 'css_gradient',
                'type'       => 'textarea_code',
            )
        );

        //Hero Subtitle
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Subtitle', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'subtitle',
                'type'       => 'text_medium',
            )
        );

        //Hero Subtitle Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Subtitle Color', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'subtitle_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#212121', '#ae0f0a', '#fbb900'),
                        ),
                    ),
                ),
            )
        );

        //Hero Title
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Title', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'title',
                'type'       => 'textarea_code',
            )
        );

        //Hero Subtitle Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Title Color', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'title_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#212121', '#ae0f0a', '#fbb900'),
                        ),
                    ),
                ),
            )
        );

        //Hero Image Inner (Desktop)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Image Inner (Desktop)', 'desktopnovainternetcombr'),
                'description' => __('PNG with transparent background', 'desktopnovainternetcombr'),
                'id'          => 'img_inner_desktop',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'desktopnovainternetcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );
        
        //Hero Image Inner (Mobile)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Image Inner (Mobile)', 'desktopnovainternetcombr'),
                'description' => __('PNG with transparent background', 'desktopnovainternetcombr'),
                'id'          => 'img_inner_mobile',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'desktopnovainternetcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );
        //Hero Button Text
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Button Text', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'btn_text',
                'type'       => 'text_medium',
            )
        );

        //Hero Button URL
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Button URL', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'btn_url',
                'type'       => 'text',
            )
        );
        
        //Hero Button CSS classes
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Button CSS Classes', 'desktopnovainternetcombr'),
                'desc'       => __('Colors: wedo-c-button--primary, wedo-c-button--secondary, wedo-c-button--white / Types: wedo-c-button--solid / wedo-c-button--big', 'desktopnovainternetcombr'),
                'id'         => 'btn_css',
                'default'    => 'wedo-c-button--secondary wedo-c-button--solid',
                'type'       => 'text',
            )
        );

        /******
         * Content
         ******/
        $cmb_content = new_cmb2_box(
            array(
                'id'            => 'desktopnovainternetcombr_combos_content_id',
                'title'         => __('Content', 'desktopnovainternetcombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'combos'),
                //'show_on'      => array( 'key' => 'page-template', 'value' => 'page-combos.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Content Title
        $cmb_content->add_field(
            array(
                'name'       => __('Title', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'content_title',
                'type'       => 'textarea_code',
            )
        );

        //Content Subtitle
        $cmb_content->add_field(
            array(
                'name'       => __('Subtitle', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'content_subtitle',
                'type'       => 'textarea_code',
            )
        );
    }
);

/**
 * Combos CPT
 */
add_action(
    'cmb2_admin_init',
    function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_desktopnovainternetcombr_combo_cpt_';

        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => 'desktopnovainternetcombr_combo_cpt_hero_id',
                'title'         => __('Hero', 'desktopnovainternetcombr'),
                'object_types'  => array('desktop_combos'), // post type
                //'show_on' => array('key' => 'slug', 'value' => 'combos'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
                
        //Hero Group
        $hero_id = $cmb_hero->add_field(
            array(
                'id'          => $prefix . 'hero_slides',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   =>__('Slide {#}', 'desktopnovainternetcombr'),
                    'add_button'   =>__('Add Another Slide', 'desktopnovainternetcombr'),
                    'remove_button' =>__('Remove Slide', 'desktopnovainternetcombr'),
                    'sortable'      => true, // beta
                ),
            )
        );

        //Background Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Background Color', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#fbfbfb', '#ae0f0a', '#fbb900'),
                        ),
                    ),
                ),
            )
        );

        //Hero Background Image (Desktop)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Desktop)', 'desktopnovainternetcombr'),
                'description' => '',
                'id'          => 'bkg_img_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'desktopnovainternetcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero Background Image (Mobile)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Mobile)', 'desktopnovainternetcombr'),
                'description' => '',
                'id'          => 'bkg_img_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'desktopnovainternetcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero CSS Gradient
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('CSS Gradient', 'desktopnovainternetcombr'),
                'desc'       => 'linear-gradient(270deg, #ac2407 0%, rgba(179, 85, 65, 0) 10.5%, rgba(179, 85, 65, 0) 89.5%, #ac2407 100%);',
                'id'         => 'css_gradient',
                'type'       => 'textarea_code',
            )
        );

        //Hero Subtitle
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Subtitle', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'subtitle',
                'type'       => 'text_medium',
            )
        );

        //Hero Subtitle Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Subtitle Color', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'subtitle_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#212121', '#ae0f0a', '#fbb900'),
                        ),
                    ),
                ),
            )
        );

        //Hero Title
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Title', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'title',
                'type'       => 'textarea_code',
            )
        );

        //Hero Subtitle Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Title Color', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'title_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#212121', '#ae0f0a', '#fbb900'),
                        ),
                    ),
                ),
            )
        );

        //Hero Image Inner (Desktop)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Image Inner (Desktop)', 'desktopnovainternetcombr'),
                'description' => __('PNG with transparent background', 'desktopnovainternetcombr'),
                'id'          => 'img_inner_desktop',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'desktopnovainternetcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );
        
        //Hero Image Inner (Mobile)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Image Inner (Mobile)', 'desktopnovainternetcombr'),
                'description' => __('PNG with transparent background', 'desktopnovainternetcombr'),
                'id'          => 'img_inner_mobile',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'desktopnovainternetcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );
        //Hero Button Text
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Button Text', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'btn_text',
                'type'       => 'text_medium',
            )
        );

        //Hero Button URL
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Button URL', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'btn_url',
                'type'       => 'text',
            )
        );
        
        //Hero Button CSS classes
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Button CSS Classes', 'desktopnovainternetcombr'),
                'desc'       => __('Colors: wedo-c-button--primary, wedo-c-button--secondary, wedo-c-button--white / Types: wedo-c-button--solid / wedo-c-button--big', 'desktopnovainternetcombr'),
                'id'         => 'btn_css',
                'default'    => 'wedo-c-button--secondary wedo-c-button--solid',
                'type'       => 'text',
            )
        );

        /******
         * Content
         ******/
        $cmb_content = new_cmb2_box(
            array(
                'id'            => 'desktopnovainternetcombr_combo_cpt_content_id',
                'title'         => __('Content', 'desktopnovainternetcombr'),
                'object_types'  => array('desktop_combos'), // post type
                //'show_on' => array('key' => 'slug', 'value' => 'combos'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Combo Title
        $cmb_content->add_field(
            array(
                'name'       => __('Title', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'content_title',
                'type'       => 'textarea_code',
            )
        );

        //Combo Subtitle
        $cmb_content->add_field(
            array(
                'name'       => __('Subtitle', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'content_subtitle',
                'type'       => 'textarea_code',
            )
        );

        /******
         * Plan
         ******/
        $cmb_plan = new_cmb2_box(
            array(
                'id'            => 'desktopnovainternetcombr_combo_cpt_plan_id',
                'title'         => __('Plan', 'desktopnovainternetcombr'),
                'object_types'  => array('desktop_combos'), // post type
                //'show_on' => array('key' => 'slug', 'value' => 'combos'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Plan Settings
        $cmb_plan->add_field( 
            array(
                'name' => __('Settings', 'desktopnovainternetcombr'),
                'desc' => '',
                'type' => 'title',
                'id'   => $prefix . 'plan_settings_title',
            ) 
        );

        //Plan Show
        $cmb_plan->add_field(
            array(
                'name'       => __('Display on front page', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'plan_show',
                'type'       => 'checkbox',
            )
        );

        //Plan Featured
        $cmb_plan->add_field(
            array(
                'name'       => __('Featured plan', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'plan_featured',
                'type'       => 'checkbox',
            )
        );

        //Plan Header
        $cmb_plan->add_field( 
            array(
                'name' => __('Header', 'desktopnovainternetcombr'),
                'desc' => '',
                'type' => 'title',
                'id'   => $prefix . 'plan_header_title',
            ) 
        );

        //Plan Label
        $cmb_plan->add_field(
            array(
                'name'       => __('Label text', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'plan_label_text',
                'type'       => 'text',
            )
        );

        //Plan Title
        $cmb_plan->add_field(
            array(
                'name'       => __('Title', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'plan_title',
                'type'       => 'text',
            )
        );

        //Plan Subtitle
        $cmb_plan->add_field(
            array(
                'name'       => __('Subtitle', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'plan_subtitle',
                'type'       => 'text',
            )
        );

        //Plan Label Description
        $cmb_plan->add_field(
            array(
                'name'       => __('Description', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'plan_desc',
                'type'       => 'text',
            )
        );

        //Plan Price
        $cmb_plan->add_field( 
            array(
                'name' => __('Price', 'desktopnovainternetcombr'),
                'desc' => '',
                'type' => 'title',
                'id'   => $prefix . 'plan_price_title',
            ) 
        );

        //Plan Price Prefix
        $cmb_plan->add_field(
            array(
                'name'       => __('Prefix', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'plan_price_prefix',
                'type'       => 'text',
                'default'    => __('From', 'desktopnovainternetcombr'),
            )
        );

        //Plan Price
        $cmb_plan->add_field(
            array(
                'name'       => __('Price', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'plan_price_unit',
                'type'       => 'text',
            )
        );

        //Plan Price Cents
        $cmb_plan->add_field(
            array(
                'name'       => __('Cents', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'plan_price_cents',
                'type'       => 'text',
                'default'    => '90',
            )
        );

        //Plan Price Period
        $cmb_plan->add_field(
            array(
                'name'       => __('Period', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'plan_price_period',
                'type'       => 'text',
                'default'    => __('month', 'desktopnovainternetcombr'),
            )
        );

        //Plan Price Suffix
        $cmb_plan->add_field(
            array(
                'name'       => __('Suffix', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'plan_price_suffix',
                'type'       => 'text',
            )
        );

        //Plan Btn Text
        $cmb_plan->add_field(
            array(
                'name'       => __('Button Text', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'plan_btn_text',
                'type'       => 'text',
                'default'    => __('Subscribe now', 'desktopnovainternetcombr'),
            )
        );

        //Plan Internet Title
        $cmb_plan->add_field(
            array(
                'name'       => __('Internet', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'plan_internet_title',
                'type'       => 'title',
            )
        );

        //Plan Internet Speed
        $cmb_plan->add_field(
            array(
                'name'       => __('Speed', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'plan_internet_speed',
                'type'       => 'text',
            )
        );

        //Plan Internet Items
        $cmb_plan->add_field(
            array(
                'name'       => __('Items', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'plan_internet_items',
                'type'       => 'textarea_code',
            )
        );

        //Plan TV Title
        $cmb_plan->add_field(
            array(
                'name'       => __('TV', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'plan_tv_title',
                'type'       => 'title',
            )
        );

        //Plan TV channels
        $cmb_plan->add_field(
            array(
                'name'       => __('Number of channels', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'plan_tv_channels',
                'type'       => 'text',
            )
        );

        //Plan TV Items
        $cmb_plan->add_field(
            array(
                'name'       => __('Items', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'plan_tv_items',
                'type'       => 'textarea_code',
            )
        );

        //Plan Phone Title
        $cmb_plan->add_field(
            array(
                'name'       => __('Phone', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'plan_phone_title',
                'type'       => 'title',
            )
        );

        //Plan Phone 
        $cmb_plan->add_field(
            array(
                'name'       => __('Show on plan', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'plan_phone_show',
                'type'       => 'checkbox',
                'default'    => false,
            )
        );

        //Plan Phone Items
        $cmb_plan->add_field(
            array(
                'name'       => __('Items', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'plan_phone_items',
                'type'       => 'textarea_code',
            )
        );

        //Plan Conditions Title
        $cmb_plan->add_field(
            array(
                'name'       => __('Conditions', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'plan_conditions_title',
                'type'       => 'title',
            )
        );

        //Plan Conditions Items
        $cmb_plan->add_field(
            array(
                'name'       => __('Items', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'plan_conditions_items',
                'type'       => 'textarea_code',
            )
        );
    }
);

/**
 * Internet Page
 */
add_action(
    'cmb2_admin_init',
    function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_desktopnovainternetcombr_internet_';

        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => 'desktopnovainternetcombr_internet_hero_id',
                'title'         => __('Hero', 'desktopnovainternetcombr'),
                'object_types'  => array('page'), // post type
                'show_on'       => array('key' => 'slug', 'value' => 'internet'),
                //'show_on'       => array( 'key' => 'page-template', 'value' => 'page-internet.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
                
        //Hero Group
        $hero_id = $cmb_hero->add_field(
            array(
                'id'          => $prefix . 'hero_slides',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   =>__('Slide {#}', 'desktopnovainternetcombr'),
                    'add_button'   =>__('Add Another Slide', 'desktopnovainternetcombr'),
                    'remove_button' =>__('Remove Slide', 'desktopnovainternetcombr'),
                    'sortable'      => true, // beta
                ),
            )
        );

        //Background Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Background Color', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#fbfbfb', '#ae0f0a', '#fbb900'),
                        ),
                    ),
                ),
            )
        );

        //Hero Background Image (Desktop)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Desktop)', 'desktopnovainternetcombr'),
                'description' => '',
                'id'          => 'bkg_img_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'desktopnovainternetcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero Background Image (Mobile)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Mobile)', 'desktopnovainternetcombr'),
                'description' => '',
                'id'          => 'bkg_img_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'desktopnovainternetcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero CSS Gradient
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('CSS Gradient', 'desktopnovainternetcombr'),
                'desc'       => 'linear-gradient(270deg, #ac2407 0%, rgba(179, 85, 65, 0) 10.5%, rgba(179, 85, 65, 0) 89.5%, #ac2407 100%);',
                'id'         => 'css_gradient',
                'type'       => 'textarea_code',
            )
        );

        //Hero Subtitle
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Subtitle', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'subtitle',
                'type'       => 'text_medium',
            )
        );

        //Hero Subtitle Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Subtitle Color', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'subtitle_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#212121', '#ae0f0a', '#fbb900'),
                        ),
                    ),
                ),
            )
        );

        //Hero Title
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Title', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'title',
                'type'       => 'textarea_code',
            )
        );

        //Hero Subtitle Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Title Color', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'title_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#212121', '#ae0f0a', '#fbb900'),
                        ),
                    ),
                ),
            )
        );

        //Hero Image Inner (Desktop)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Image Inner (Desktop)', 'desktopnovainternetcombr'),
                'description' => __('PNG with transparent background', 'desktopnovainternetcombr'),
                'id'          => 'img_inner_desktop',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'desktopnovainternetcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );
        
        //Hero Image Inner (Mobile)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Image Inner (Mobile)', 'desktopnovainternetcombr'),
                'description' => __('PNG with transparent background', 'desktopnovainternetcombr'),
                'id'          => 'img_inner_mobile',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'desktopnovainternetcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );
        //Hero Button Text
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Button Text', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'btn_text',
                'type'       => 'text_medium',
            )
        );

        //Hero Button URL
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Button URL', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'btn_url',
                'type'       => 'text',
            )
        );
        
        //Hero Button CSS classes
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Button CSS Classes', 'desktopnovainternetcombr'),
                'desc'       => __('Colors: wedo-c-button--primary, wedo-c-button--secondary, wedo-c-button--white / Types: wedo-c-button--solid / wedo-c-button--big', 'desktopnovainternetcombr'),
                'id'         => 'btn_css',
                'default'    => 'wedo-c-button--secondary wedo-c-button--solid',
                'type'       => 'text',
            )
        );

        /******
         * Content
         ******/
        $cmb_content = new_cmb2_box(
            array(
                'id'            => 'desktopnovainternetcombr_internet_content_id',
                'title'         => __('Content', 'desktopnovainternetcombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'internet'),
                //'show_on'      => array( 'key' => 'page-template', 'value' => 'page-internet.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Content Title
        $cmb_content->add_field(
            array(
                'name'       => __('Title', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'content_title',
                'type'       => 'textarea_code',
            )
        );

        //Content Subtitle
        $cmb_content->add_field(
            array(
                'name'       => __('Subtitle', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'content_subtitle',
                'type'       => 'textarea_code',
            )
        );

        
    }
);

/**
 * Internet CPT
 */
add_action(
    'cmb2_admin_init',
    function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_desktopnovainternetcombr_internet_cpt_';

        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => 'desktopnovainternetcombr_internet_cpt_hero_id',
                'title'         => __('Hero', 'desktopnovainternetcombr'),
                'object_types'  => array('desktop_internet'), // post type
                //'show_on' => array('key' => 'slug', 'value' => 'internet'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
                
        //Hero Group
        $hero_id = $cmb_hero->add_field(
            array(
                'id'          => $prefix . 'hero_slides',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   =>__('Slide {#}', 'desktopnovainternetcombr'),
                    'add_button'   =>__('Add Another Slide', 'desktopnovainternetcombr'),
                    'remove_button' =>__('Remove Slide', 'desktopnovainternetcombr'),
                    'sortable'      => true, // beta
                ),
            )
        );

        //Background Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Background Color', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#fbfbfb', '#ae0f0a', '#fbb900'),
                        ),
                    ),
                ),
            )
        );

        //Hero Background Image (Desktop)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Desktop)', 'desktopnovainternetcombr'),
                'description' => '',
                'id'          => 'bkg_img_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'desktopnovainternetcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero Background Image (Mobile)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Mobile)', 'desktopnovainternetcombr'),
                'description' => '',
                'id'          => 'bkg_img_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'desktopnovainternetcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero CSS Gradient
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('CSS Gradient', 'desktopnovainternetcombr'),
                'desc'       => 'linear-gradient(270deg, #ac2407 0%, rgba(179, 85, 65, 0) 10.5%, rgba(179, 85, 65, 0) 89.5%, #ac2407 100%);',
                'id'         => 'css_gradient',
                'type'       => 'textarea_code',
            )
        );

        //Hero Subtitle
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Subtitle', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'subtitle',
                'type'       => 'text_medium',
            )
        );

        //Hero Subtitle Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Subtitle Color', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'subtitle_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#212121', '#ae0f0a', '#fbb900'),
                        ),
                    ),
                ),
            )
        );

        //Hero Title
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Title', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'title',
                'type'       => 'textarea_code',
            )
        );

        //Hero Subtitle Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Title Color', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'title_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#212121', '#ae0f0a', '#fbb900'),
                        ),
                    ),
                ),
            )
        );

        //Hero Image Inner (Desktop)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Image Inner (Desktop)', 'desktopnovainternetcombr'),
                'description' => __('PNG with transparent background', 'desktopnovainternetcombr'),
                'id'          => 'img_inner_desktop',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'desktopnovainternetcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );
        
        //Hero Image Inner (Mobile)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Image Inner (Mobile)', 'desktopnovainternetcombr'),
                'description' => __('PNG with transparent background', 'desktopnovainternetcombr'),
                'id'          => 'img_inner_mobile',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'desktopnovainternetcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );
        //Hero Button Text
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Button Text', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'btn_text',
                'type'       => 'text_medium',
            )
        );

        //Hero Button URL
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Button URL', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'btn_url',
                'type'       => 'text',
            )
        );
        
        //Hero Button CSS classes
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Button CSS Classes', 'desktopnovainternetcombr'),
                'desc'       => __('Colors: wedo-c-button--primary, wedo-c-button--secondary, wedo-c-button--white / Types: wedo-c-button--solid / wedo-c-button--big', 'desktopnovainternetcombr'),
                'id'         => 'btn_css',
                'default'    => 'wedo-c-button--secondary wedo-c-button--solid',
                'type'       => 'text',
            )
        );

        /******
         * Content
         ******/
        $cmb_content = new_cmb2_box(
            array(
                'id'            => 'desktopnovainternetcombr_internet_cpt_content_id',
                'title'         => __('Content', 'desktopnovainternetcombr'),
                'object_types'  => array('desktop_internet'), // post type
                //'show_on' => array('key' => 'slug', 'value' => 'internet'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Content Title
        $cmb_content->add_field(
            array(
                'name'       => __('Title', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'content_title',
                'type'       => 'textarea_code',
            )
        );

        //Content Subtitle
        $cmb_content->add_field(
            array(
                'name'       => __('Subtitle', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'content_subtitle',
                'type'       => 'textarea_code',
            )
        );

        /******
         * Plan
         ******/
        $cmb_plan = new_cmb2_box(
            array(
                'id'            => 'desktopnovainternetcombr_internet_cpt_plan_id',
                'title'         => __('Plan', 'desktopnovainternetcombr'),
                'object_types'  => array('desktop_internet'), // post type
                //'show_on' => array('key' => 'slug', 'value' => 'combos'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Plan Settings
        $cmb_plan->add_field( 
            array(
                'name' => __('Settings', 'desktopnovainternetcombr'),
                'desc' => '',
                'type' => 'title',
                'id'   => $prefix . 'plan_settings_title',
            ) 
        );

        //Plan Show
        $cmb_plan->add_field(
            array(
                'name'       => __('Display on front page', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'plan_show',
                'type'       => 'checkbox',
            )
        );

        //Plan Featured
        $cmb_plan->add_field(
            array(
                'name'       => __('Featured plan', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'plan_featured',
                'type'       => 'checkbox',
            )
        );

        //Plan Header
        $cmb_plan->add_field( 
            array(
                'name' => __('Header', 'desktopnovainternetcombr'),
                'desc' => '',
                'type' => 'title',
                'id'   => $prefix . 'plan_header_title',
            ) 
        );

        //Plan Label
        $cmb_plan->add_field(
            array(
                'name'       => __('Label text', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'plan_label_text',
                'type'       => 'text',
            )
        );

        //Plan Title
        $cmb_plan->add_field(
            array(
                'name'       => __('Title', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'plan_title',
                'type'       => 'text',
            )
        );

        //Plan Subtitle
        $cmb_plan->add_field(
            array(
                'name'       => __('Subtitle', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'plan_subtitle',
                'type'       => 'text',
            )
        );

        //Plan Label Description
        $cmb_plan->add_field(
            array(
                'name'       => __('Description', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'plan_desc',
                'type'       => 'text',
            )
        );

        //Plan Price
        $cmb_plan->add_field( 
            array(
                'name' => __('Price', 'desktopnovainternetcombr'),
                'desc' => '',
                'type' => 'title',
                'id'   => $prefix . 'plan_price_title',
            ) 
        );

        //Plan Price Prefix
        $cmb_plan->add_field(
            array(
                'name'       => __('Prefix', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'plan_price_prefix',
                'type'       => 'text',
                'default'    => __('From', 'desktopnovainternetcombr'),
            )
        );

        //Plan Price
        $cmb_plan->add_field(
            array(
                'name'       => __('Price', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'plan_price_unit',
                'type'       => 'text',
            )
        );

        //Plan Price Cents
        $cmb_plan->add_field(
            array(
                'name'       => __('Cents', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'plan_price_cents',
                'type'       => 'text',
                'default'    => '90',
            )
        );

        //Plan Price Period
        $cmb_plan->add_field(
            array(
                'name'       => __('Period', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'plan_price_period',
                'type'       => 'text',
                'default'    => __('month', 'desktopnovainternetcombr'),
            )
        );

        //Plan Price Suffix
        $cmb_plan->add_field(
            array(
                'name'       => __('Suffix', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'plan_price_suffix',
                'type'       => 'text',
            )
        );

        //Plan Btn Text
        $cmb_plan->add_field(
            array(
                'name'       => __('Button Text', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'plan_btn_text',
                'type'       => 'text',
                'default'    => __('Subscribe now', 'desktopnovainternetcombr'),
            )
        );

        //Plan Internet Title
        $cmb_plan->add_field(
            array(
                'name'       => __('Internet', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'plan_internet_title',
                'type'       => 'title',
            )
        );

        //Plan Internet Speed
        $cmb_plan->add_field(
            array(
                'name'       => __('Speed', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'plan_internet_speed',
                'type'       => 'text',
            )
        );

        //Plan Internet Items
        $cmb_plan->add_field(
            array(
                'name'       => __('Items', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'plan_internet_items',
                'type'       => 'textarea_code',
            )
        );

        //Plan Phone Title
        $cmb_plan->add_field(
            array(
                'name'       => __('Phone', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'plan_phone_title',
                'type'       => 'title',
            )
        );

        //Plan Phone 
        $cmb_plan->add_field(
            array(
                'name'       => __('Show on plan', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'plan_phone_show',
                'type'       => 'checkbox',
                'default'    => false,
            )
        );

        //Plan Phone Items
        $cmb_plan->add_field(
            array(
                'name'       => __('Items', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'plan_phone_items',
                'type'       => 'textarea_code',
            )
        );

        //Plan Conditions Title
        $cmb_plan->add_field(
            array(
                'name'       => __('Conditions', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'plan_conditions_title',
                'type'       => 'title',
            )
        );

        //Plan Conditions Items
        $cmb_plan->add_field(
            array(
                'name'       => __('Items', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'plan_conditions_items',
                'type'       => 'textarea_code',
            )
        );
    }
);

/**
 * Contact Page
 */
add_action(
    'cmb2_admin_init',
    function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_desktopnovainternetcombr_contato_';

        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => 'desktopnovainternetcombr_contato_hero_id',
                'title'         => __('Hero', 'desktopnovainternetcombr'),
                'object_types'  => array('page'), // post type
                'show_on'       => array('key' => 'slug', 'value' => 'fale-conosco'),
                //'show_on'       => array( 'key' => 'page-template', 'value' => 'page-fale-conosco.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
                
        //Hero Group
        $hero_id = $cmb_hero->add_field(
            array(
                'id'          => $prefix . 'hero_slides',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   =>__('Slide {#}', 'desktopnovainternetcombr'),
                    'add_button'   =>__('Add Another Slide', 'desktopnovainternetcombr'),
                    'remove_button' =>__('Remove Slide', 'desktopnovainternetcombr'),
                    'sortable'      => true, // beta
                ),
            )
        );

        //Background Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Background Color', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#fbfbfb', '#ae0f0a', '#fbb900'),
                        ),
                    ),
                ),
            )
        );

        //Hero Background Image (Desktop)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Desktop)', 'desktopnovainternetcombr'),
                'description' => '',
                'id'          => 'bkg_img_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'desktopnovainternetcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero Background Image (Mobile)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Mobile)', 'desktopnovainternetcombr'),
                'description' => '',
                'id'          => 'bkg_img_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'desktopnovainternetcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero CSS Gradient
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('CSS Gradient', 'desktopnovainternetcombr'),
                'desc'       => 'linear-gradient(270deg, #ac2407 0%, rgba(179, 85, 65, 0) 10.5%, rgba(179, 85, 65, 0) 89.5%, #ac2407 100%);',
                'id'         => 'css_gradient',
                'type'       => 'textarea_code',
            )
        );

        //Hero Subtitle
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Subtitle', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'subtitle',
                'type'       => 'text_medium',
            )
        );

        //Hero Subtitle Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Subtitle Color', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'subtitle_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#212121', '#ae0f0a', '#fbb900'),
                        ),
                    ),
                ),
            )
        );

        //Hero Title
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Title', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'title',
                'type'       => 'textarea_code',
            )
        );

        //Hero Subtitle Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Title Color', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'title_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#212121', '#ae0f0a', '#fbb900'),
                        ),
                    ),
                ),
            )
        );

        //Hero Image Inner (Desktop)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Image Inner (Desktop)', 'desktopnovainternetcombr'),
                'description' => __('PNG with transparent background', 'desktopnovainternetcombr'),
                'id'          => 'img_inner_desktop',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'desktopnovainternetcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );
        
        //Hero Image Inner (Mobile)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Image Inner (Mobile)', 'desktopnovainternetcombr'),
                'description' => __('PNG with transparent background', 'desktopnovainternetcombr'),
                'id'          => 'img_inner_mobile',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'desktopnovainternetcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );
        //Hero Button Text
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Button Text', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'btn_text',
                'type'       => 'text_medium',
            )
        );

        //Hero Button URL
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Button URL', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'btn_url',
                'type'       => 'text',
            )
        );
        
        //Hero Button CSS classes
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Button CSS Classes', 'desktopnovainternetcombr'),
                'desc'       => __('Colors: wedo-c-button--primary, wedo-c-button--secondary, wedo-c-button--white / Types: wedo-c-button--solid / wedo-c-button--big', 'desktopnovainternetcombr'),
                'id'         => 'btn_css',
                'default'    => 'wedo-c-button--secondary wedo-c-button--solid',
                'type'       => 'text',
            )
        );

        /******
         * Content
         ******/
        $cmb_content = new_cmb2_box(
            array(
                'id'            => 'desktopnovainternetcombr_contato_content_id',
                'title'         => __('Content', 'desktopnovainternetcombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'fale-conosco'),
                //'show_on'      => array( 'key' => 'page-template', 'value' => 'page-fale-conosco.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Content Title
        $cmb_content->add_field(
            array(
                'name'       => __('Title', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'content_title',
                'type'       => 'textarea_code',
            )
        );

        //Content Subtitle
        $cmb_content->add_field(
            array(
                'name'       => __('Subtitle', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'content_subtitle',
                'type'       => 'textarea_code',
            )
        );

        //Shortcode
        $cmb_content->add_field(
            array(
                'name'       => __('Form Shortcode', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'content_form_shortcode',
                'type'       => 'text',
            )
        );
    }
);

/**
 * Para Sua Casa Page
 */
add_action(
    'cmb2_admin_init',
    function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_desktopnovainternetcombr_para-sua-casa_';

        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => 'desktopnovainternetcombr_para-sua-casa_hero_id',
                'title'         => __('Hero', 'desktopnovainternetcombr'),
                'object_types'  => array('page'), // post type
                'show_on'       => array('key' => 'slug', 'value' => 'para-sua-casa'),
                //'show_on'       => array( 'key' => 'page-template', 'value' => 'page-para-sua-casa.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
                
        //Hero Group
        $hero_id = $cmb_hero->add_field(
            array(
                'id'          => $prefix . 'hero_slides',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   =>__('Slide {#}', 'desktopnovainternetcombr'),
                    'add_button'   =>__('Add Another Slide', 'desktopnovainternetcombr'),
                    'remove_button' =>__('Remove Slide', 'desktopnovainternetcombr'),
                    'sortable'      => true, // beta
                ),
            )
        );

        //Background Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Background Color', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#fbfbfb', '#ae0f0a', '#fbb900'),
                        ),
                    ),
                ),
            )
        );

        //Hero Background Image (Desktop)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Desktop)', 'desktopnovainternetcombr'),
                'description' => '',
                'id'          => 'bkg_img_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'desktopnovainternetcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero Background Image (Mobile)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Mobile)', 'desktopnovainternetcombr'),
                'description' => '',
                'id'          => 'bkg_img_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'desktopnovainternetcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero CSS Gradient
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('CSS Gradient', 'desktopnovainternetcombr'),
                'desc'       => 'linear-gradient(270deg, #ac2407 0%, rgba(179, 85, 65, 0) 10.5%, rgba(179, 85, 65, 0) 89.5%, #ac2407 100%);',
                'id'         => 'css_gradient',
                'type'       => 'textarea_code',
            )
        );

        //Hero Subtitle
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Subtitle', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'subtitle',
                'type'       => 'text_medium',
            )
        );

        //Hero Subtitle Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Subtitle Color', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'subtitle_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#212121', '#ae0f0a', '#fbb900'),
                        ),
                    ),
                ),
            )
        );

        //Hero Title
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Title', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'title',
                'type'       => 'textarea_code',
            )
        );

        //Hero Subtitle Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Title Color', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'title_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#212121', '#ae0f0a', '#fbb900'),
                        ),
                    ),
                ),
            )
        );

        //Hero Image Inner (Desktop)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Image Inner (Desktop)', 'desktopnovainternetcombr'),
                'description' => __('PNG with transparent background', 'desktopnovainternetcombr'),
                'id'          => 'img_inner_desktop',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'desktopnovainternetcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );
        
        //Hero Image Inner (Mobile)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Image Inner (Mobile)', 'desktopnovainternetcombr'),
                'description' => __('PNG with transparent background', 'desktopnovainternetcombr'),
                'id'          => 'img_inner_mobile',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'desktopnovainternetcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );
        //Hero Button Text
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Button Text', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'btn_text',
                'type'       => 'text_medium',
            )
        );

        //Hero Button URL
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Button URL', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'btn_url',
                'type'       => 'text',
            )
        );
        
        //Hero Button CSS classes
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Button CSS Classes', 'desktopnovainternetcombr'),
                'desc'       => __('Colors: wedo-c-button--primary, wedo-c-button--secondary, wedo-c-button--white / Types: wedo-c-button--solid / wedo-c-button--big', 'desktopnovainternetcombr'),
                'id'         => 'btn_css',
                'default'    => 'wedo-c-button--secondary wedo-c-button--solid',
                'type'       => 'text',
            )
        );

        /******
         * Content
         ******/
        $cmb_content = new_cmb2_box(
            array(
                'id'            => 'desktopnovainternetcombr_para-sua-casa_content_id',
                'title'         => __('Content', 'desktopnovainternetcombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'para-sua-casa'),
                //'show_on'      => array( 'key' => 'page-template', 'value' => 'page-para-sua-casa.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Content Title
        $cmb_content->add_field(
            array(
                'name'       => __('Title', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'content_title',
                'type'       => 'textarea_code',
            )
        );

        //Content Subtitle
        $cmb_content->add_field(
            array(
                'name'       => __('Subtitle', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'content_subtitle',
                'type'       => 'textarea_code',
            )
        );
    }
);

/**
 * Para Sua Empresa Page
 */
add_action(
    'cmb2_admin_init',
    function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_desktopnovainternetcombr_para-sua-empresa_';

        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => 'desktopnovainternetcombr_para-sua-empresa_hero_id',
                'title'         => __('Hero', 'desktopnovainternetcombr'),
                'object_types'  => array('page'), // post type
                'show_on'       => array('key' => 'slug', 'value' => 'para-sua-empresa'),
                //'show_on'       => array( 'key' => 'page-template', 'value' => 'page-para-sua-empresa.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
                
        //Hero Group
        $hero_id = $cmb_hero->add_field(
            array(
                'id'          => $prefix . 'hero_slides',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   =>__('Slide {#}', 'desktopnovainternetcombr'),
                    'add_button'   =>__('Add Another Slide', 'desktopnovainternetcombr'),
                    'remove_button' =>__('Remove Slide', 'desktopnovainternetcombr'),
                    'sortable'      => true, // beta
                ),
            )
        );

        //Background Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Background Color', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#fbfbfb', '#ae0f0a', '#fbb900'),
                        ),
                    ),
                ),
            )
        );

        //Hero Background Image (Desktop)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Desktop)', 'desktopnovainternetcombr'),
                'description' => '',
                'id'          => 'bkg_img_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'desktopnovainternetcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero Background Image (Mobile)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Mobile)', 'desktopnovainternetcombr'),
                'description' => '',
                'id'          => 'bkg_img_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'desktopnovainternetcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero CSS Gradient
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('CSS Gradient', 'desktopnovainternetcombr'),
                'desc'       => 'linear-gradient(270deg, #ac2407 0%, rgba(179, 85, 65, 0) 10.5%, rgba(179, 85, 65, 0) 89.5%, #ac2407 100%);',
                'id'         => 'css_gradient',
                'type'       => 'textarea_code',
            )
        );

        //Hero Subtitle
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Subtitle', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'subtitle',
                'type'       => 'text_medium',
            )
        );

        //Hero Subtitle Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Subtitle Color', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'subtitle_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#212121', '#ae0f0a', '#fbb900'),
                        ),
                    ),
                ),
            )
        );

        //Hero Title
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Title', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'title',
                'type'       => 'textarea_code',
            )
        );

        //Hero Subtitle Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Title Color', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'title_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#212121', '#ae0f0a', '#fbb900'),
                        ),
                    ),
                ),
            )
        );

        //Hero Image Inner (Desktop)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Image Inner (Desktop)', 'desktopnovainternetcombr'),
                'description' => __('PNG with transparent background', 'desktopnovainternetcombr'),
                'id'          => 'img_inner_desktop',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'desktopnovainternetcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );
        
        //Hero Image Inner (Mobile)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Image Inner (Mobile)', 'desktopnovainternetcombr'),
                'description' => __('PNG with transparent background', 'desktopnovainternetcombr'),
                'id'          => 'img_inner_mobile',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'desktopnovainternetcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );
        //Hero Button Text
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Button Text', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'btn_text',
                'type'       => 'text_medium',
            )
        );

        //Hero Button URL
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Button URL', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'btn_url',
                'type'       => 'text',
            )
        );
        
        //Hero Button CSS classes
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Button CSS Classes', 'desktopnovainternetcombr'),
                'desc'       => __('Colors: wedo-c-button--primary, wedo-c-button--secondary, wedo-c-button--white / Types: wedo-c-button--solid / wedo-c-button--big', 'desktopnovainternetcombr'),
                'id'         => 'btn_css',
                'default'    => 'wedo-c-button--secondary wedo-c-button--solid',
                'type'       => 'text',
            )
        );

        /******
         * Content
         ******/
        $cmb_content = new_cmb2_box(
            array(
                'id'            => 'desktopnovainternetcombr_para-sua-empresa_content_id',
                'title'         => __('Content', 'desktopnovainternetcombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'para-sua-empresa'),
                //'show_on'      => array( 'key' => 'page-template', 'value' => 'page-para-sua-empresa.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Content Title
        $cmb_content->add_field(
            array(
                'name'       => __('Title', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'content_title',
                'type'       => 'textarea_code',
            )
        );

        //Content Subtitle
        $cmb_content->add_field(
            array(
                'name'       => __('Subtitle', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'content_subtitle',
                'type'       => 'textarea_code',
            )
        );
    }
);

/**
 * Telefone
 */
add_action(
    'cmb2_admin_init',
    function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_desktopnovainternetcombr_telefone_';

        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => 'desktopnovainternetcombr_telefone_hero_id',
                'title'         => __('Hero', 'desktopnovainternetcombr'),
                'object_types'  => array('page'), // post type
                'show_on'       => array('key' => 'slug', 'value' => 'telefone'),
                //'show_on'       => array( 'key' => 'page-template', 'value' => 'page-telefone.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
                
        //Hero Group
        $hero_id = $cmb_hero->add_field(
            array(
                'id'          => $prefix . 'hero_slides',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   =>__('Slide {#}', 'desktopnovainternetcombr'),
                    'add_button'   =>__('Add Another Slide', 'desktopnovainternetcombr'),
                    'remove_button' =>__('Remove Slide', 'desktopnovainternetcombr'),
                    'sortable'      => true, // beta
                ),
            )
        );

        //Background Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Background Color', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#fbfbfb', '#ae0f0a', '#fbb900'),
                        ),
                    ),
                ),
            )
        );

        //Hero Background Image (Desktop)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Desktop)', 'desktopnovainternetcombr'),
                'description' => '',
                'id'          => 'bkg_img_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'desktopnovainternetcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero Background Image (Mobile)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Mobile)', 'desktopnovainternetcombr'),
                'description' => '',
                'id'          => 'bkg_img_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'desktopnovainternetcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero CSS Gradient
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('CSS Gradient', 'desktopnovainternetcombr'),
                'desc'       => 'linear-gradient(270deg, #ac2407 0%, rgba(179, 85, 65, 0) 10.5%, rgba(179, 85, 65, 0) 89.5%, #ac2407 100%);',
                'id'         => 'css_gradient',
                'type'       => 'textarea_code',
            )
        );

        //Hero Subtitle
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Subtitle', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'subtitle',
                'type'       => 'text_medium',
            )
        );

        //Hero Subtitle Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Subtitle Color', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'subtitle_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#212121', '#ae0f0a', '#fbb900'),
                        ),
                    ),
                ),
            )
        );

        //Hero Title
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Title', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'title',
                'type'       => 'textarea_code',
            )
        );

        //Hero Subtitle Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Title Color', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'title_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#212121', '#ae0f0a', '#fbb900'),
                        ),
                    ),
                ),
            )
        );

        //Hero Image Inner (Desktop)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Image Inner (Desktop)', 'desktopnovainternetcombr'),
                'description' => __('PNG with transparent background', 'desktopnovainternetcombr'),
                'id'          => 'img_inner_desktop',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'desktopnovainternetcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );
        
        //Hero Image Inner (Mobile)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Image Inner (Mobile)', 'desktopnovainternetcombr'),
                'description' => __('PNG with transparent background', 'desktopnovainternetcombr'),
                'id'          => 'img_inner_mobile',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'desktopnovainternetcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );
        //Hero Button Text
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Button Text', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'btn_text',
                'type'       => 'text_medium',
            )
        );

        //Hero Button URL
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Button URL', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'btn_url',
                'type'       => 'text',
            )
        );
        
        //Hero Button CSS classes
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Button CSS Classes', 'desktopnovainternetcombr'),
                'desc'       => __('Colors: wedo-c-button--primary, wedo-c-button--secondary, wedo-c-button--white / Types: wedo-c-button--solid / wedo-c-button--big', 'desktopnovainternetcombr'),
                'id'         => 'btn_css',
                'default'    => 'wedo-c-button--secondary wedo-c-button--solid',
                'type'       => 'text',
            )
        );

        /******
         * Content
         ******/
        $cmb_content = new_cmb2_box(
            array(
                'id'            => 'desktopnovainternetcombr_telefone_content_id',
                'title'         => __('Content', 'desktopnovainternetcombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'telefone'),
                //'show_on'      => array( 'key' => 'page-template', 'value' => 'page-telefone.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Content Title
        $cmb_content->add_field(
            array(
                'name'       => __('Title', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'content_title',
                'type'       => 'textarea_code',
            )
        );

        //Content Subtitle
        $cmb_content->add_field(
            array(
                'name'       => __('Subtitle', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'content_subtitle',
                'type'       => 'textarea_code',
            )
        );
    }
);

/**
 * TV
 */
add_action(
    'cmb2_admin_init',
    function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_desktopnovainternetcombr_tv_';

        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => 'desktopnovainternetcombr_tv_hero_id',
                'title'         => __('Hero', 'desktopnovainternetcombr'),
                'object_types'  => array('page'), // post type
                'show_on'       => array('key' => 'slug', 'value' => 'tv-digital'),
                //'show_on'       => array( 'key' => 'page-template', 'value' => 'page-tv.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
                
        //Hero Group
        $hero_id = $cmb_hero->add_field(
            array(
                'id'          => $prefix . 'hero_slides',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   =>__('Slide {#}', 'desktopnovainternetcombr'),
                    'add_button'   =>__('Add Another Slide', 'desktopnovainternetcombr'),
                    'remove_button' =>__('Remove Slide', 'desktopnovainternetcombr'),
                    'sortable'      => true, // beta
                ),
            )
        );

        //Background Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Background Color', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#fbfbfb', '#ae0f0a', '#fbb900'),
                        ),
                    ),
                ),
            )
        );

        //Hero Background Image (Desktop)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Desktop)', 'desktopnovainternetcombr'),
                'description' => '',
                'id'          => 'bkg_img_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'desktopnovainternetcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero Background Image (Mobile)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Mobile)', 'desktopnovainternetcombr'),
                'description' => '',
                'id'          => 'bkg_img_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'desktopnovainternetcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero CSS Gradient
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('CSS Gradient', 'desktopnovainternetcombr'),
                'desc'       => 'linear-gradient(270deg, #ac2407 0%, rgba(179, 85, 65, 0) 10.5%, rgba(179, 85, 65, 0) 89.5%, #ac2407 100%);',
                'id'         => 'css_gradient',
                'type'       => 'textarea_code',
            )
        );

        //Hero Subtitle
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Subtitle', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'subtitle',
                'type'       => 'text_medium',
            )
        );

        //Hero Subtitle Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Subtitle Color', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'subtitle_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#212121', '#ae0f0a', '#fbb900'),
                        ),
                    ),
                ),
            )
        );

        //Hero Title
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Title', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'title',
                'type'       => 'textarea_code',
            )
        );

        //Hero Subtitle Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Title Color', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'title_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#212121', '#ae0f0a', '#fbb900'),
                        ),
                    ),
                ),
            )
        );

        //Hero Image Inner (Desktop)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Image Inner (Desktop)', 'desktopnovainternetcombr'),
                'description' => __('PNG with transparent background', 'desktopnovainternetcombr'),
                'id'          => 'img_inner_desktop',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'desktopnovainternetcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );
        
        //Hero Image Inner (Mobile)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Image Inner (Mobile)', 'desktopnovainternetcombr'),
                'description' => __('PNG with transparent background', 'desktopnovainternetcombr'),
                'id'          => 'img_inner_mobile',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'desktopnovainternetcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );
        //Hero Button Text
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Button Text', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'btn_text',
                'type'       => 'text_medium',
            )
        );

        //Hero Button URL
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Button URL', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => 'btn_url',
                'type'       => 'text',
            )
        );
        
        //Hero Button CSS classes
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Button CSS Classes', 'desktopnovainternetcombr'),
                'desc'       => __('Colors: wedo-c-button--primary, wedo-c-button--secondary, wedo-c-button--white / Types: wedo-c-button--solid / wedo-c-button--big', 'desktopnovainternetcombr'),
                'id'         => 'btn_css',
                'default'    => 'wedo-c-button--secondary wedo-c-button--solid',
                'type'       => 'text',
            )
        );

        /******
         * Content
         ******/
        $cmb_content = new_cmb2_box(
            array(
                'id'            => 'desktopnovainternetcombr_tv_content_id',
                'title'         => __('Content', 'desktopnovainternetcombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'tv-digital'),
                //'show_on'      => array( 'key' => 'page-template', 'value' => 'page-tv.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Content Title
        $cmb_content->add_field(
            array(
                'name'       => __('Title', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'content_title',
                'type'       => 'textarea_code',
            )
        );

        //Content Subtitle
        $cmb_content->add_field(
            array(
                'name'       => __('Subtitle', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'content_subtitle',
                'type'       => 'textarea_code',
            )
        );
    }
);


/**
 * Termos e Condições
 */
add_action(
    'cmb2_admin_init',
    function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_desktopnovainternetcombr_conditions_';

        /******
         * Content
         ******/
        $cmb_content = new_cmb2_box(
            array(
                'id'            => 'desktopnovainternetcombr_conditions_content_id',
                'title'         => __('Content', 'desktopnovainternetcombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'termos-e-condicoes'),
                //'show_on'      => array( 'key' => 'page-template', 'value' => 'page-tv.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Content Title
        $cmb_content->add_field(
            array(
                'name'       => __('Title', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'content_title',
                'type'       => 'textarea_code',
            )
        );

        //Content Subtitle
        $cmb_content->add_field(
            array(
                'name'       => __('Subtitle', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'content_subtitle',
                'type'       => 'textarea_code',
            )
        );

        //Conditions Items
        $cmb_content->add_field(
            array(
                'name'       => __('Items', 'desktopnovainternetcombr'),
                'desc'       => '',
                'id'         => $prefix . 'conditions_items',
                'type'       => 'textarea_code',
            )
        );
    }
);
